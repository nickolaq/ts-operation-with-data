"use strict";
exports.__esModule = true;
var data = require('./data.js');
var Games = /** @class */ (function () {
    function Games() {
    }
    Games.filterGamesByRatio = function (ratio) {
        if (ratio === void 0) { ratio = '16:9'; }
        this.gamesList = this.games.filter(function (el) {
            return el.AR === ratio;
        });
        return this.gamesList;
    };
    Games.filterGamesByAlias = function (alias, code) {
        var _this = this;
        if (code === void 0) { code = 'en'; }
        var listOfGames = this.gamesList || this.games;
        var listOfCategories = [];
        var merchantId;
        //преобразуем массив в категорий в объект с удобным доступом, где поиск будет object[key]
        if (Object.keys(this.objectOfCategories).length === 0) {
            this.categories.forEach(function (el) {
                _this.objectOfCategories[el.ID] = el;
            });
        }
        //получаем ID конкретного мерчанта
        for (var key in this.merchants) {
            if (this.merchants[key].Alias === alias) {
                merchantId = this.merchants[key].ID;
                break;
            }
        }
        //если передали пустой параметр, то вернет все игры
        if (alias !== undefined) {
            listOfGames = listOfGames.filter(function (el) {
                return el.MerchantID === merchantId;
            });
        }
        //создаем новый массив
        listOfGames.forEach(function (el) {
            var _translate = _this.objectOfCategories[el.CategoryID[0]];
            if (_translate !== undefined) {
                listOfCategories.push(_translate.Trans[code]);
            }
        });
        console.log(listOfCategories);
        return listOfCategories;
    };
    Games.games = data.games;
    Games.merchants = data.merchants;
    Games.categories = data.categories;
    Games.objectOfCategories = {};
    return Games;
}());
exports.Games = Games;
Games.filterGamesByAlias('NetEnt', 'ru');
