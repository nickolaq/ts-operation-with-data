const data = require('./data.js');

export class Games {

    static games: object[] = data.games;
    static gamesList: object[];
    static merchants: object = data.merchants;
    static categories: object[] = data.categories;
    static objectOfCategories: object = {};

    public static filterGamesByRatio(ratio: string = '16:9'): object[] {
        this.gamesList = this.games.filter((el) => {
            return el.AR === ratio;
        });
        return this.gamesList;
    }

    public static filterGamesByAlias(alias: string, code: string = 'en'): string[] {
        const listOfGames = this.gamesList || this.games;
        const listOfCategories: string[] = [];
        let merchantId: string;

        //преобразуем массив в категорий в объект с удобным доступом, где поиск будет object[key]
        if (Object.keys(this.objectOfCategories).length === 0) {
            this.categories.forEach(el => {
                this.objectOfCategories[el.ID] = el;
            });
        }

        //получаем ID конкретного мерчанта
        for (let key in this.merchants) {
            if (this.merchants[key].Alias === alias) {
                merchantId = this.merchants[key].ID;
                break;
            }
        }

        //если передали пустой параметр, то вернет все игры
        if (alias !== undefined) {
            listOfGames = listOfGames.filter(el => {
                return el.MerchantID === merchantId;
            });
        }

        //создаем новый массив
        listOfGames.forEach(el => {
            let _translate = this.objectOfCategories[el.CategoryID[0]];
            if (_translate !== undefined) {
                listOfCategories.push(_translate.Trans[code]);
            }
        });

        console.log(listOfCategories);
        return listOfCategories;
    }
}

Games.filterGamesByAlias('NetEnt', 'ru');
